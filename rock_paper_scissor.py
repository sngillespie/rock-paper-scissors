rock = '''
    _______
---'   ____)
      (_____)
      (_____)
      (____)
---.__(___)
'''

paper = '''
    _______
---'   ____)____
          ______)
          _______)
         _______)
---.__________)
'''

scissors = '''
    _______
---'   ____)____
          ______)
       __________)
      (____)
---.__(___)
'''

#Write your code below this line 👇
import random

choice = input("What do you choose? Type 0 for Rock, 1 for Paper or 2 for Scissors.\n")

outcomes = ["0","1","2"]
computer_choice = random.choice(outcomes)

if choice == "0":
  print(rock)
  if computer_choice == "0":
    print(rock)
    print("You tie!")
  elif computer_choice == "1":
    print(paper)
    print("You lose!")
  elif computer_choice == "2":
    print(scissors)
    print("You win!")
elif choice == "1":
  print(paper)
  if computer_choice == "0":
    print(rock)
    print("You win!")
  elif computer_choice == "1":
    print(paper)
    print("You tie!")
  elif computer_choice == "2":
    print(scissors)
    print("You lose!")
else:
  print(scissors)
  if computer_choice == "0":
    print(rock)
    print("You lose!")
  elif computer_choice == "1":
    print(paper)
    print("You win!")
  elif computer_choice == "2":
    print(scissors)
    print("You tie!")
